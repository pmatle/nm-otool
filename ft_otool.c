/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_otool.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/19 12:34:51 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 09:32:31 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void	print_text64(void *ptr, void *seg, int count)
{
	int					x;
	struct section_64	*sect;

	x = 0;
	sect = (void *)seg;
	while (x < count)
	{
		if (ft_strcmp(sect->sectname, "__text") == 0)
		{
			ft_putendl("Contents of (__TEXT,__text) section");
			print_addresses64(ptr + sect->offset, sect);
			break ;
		}
		x++;
		sect = (void *)sect + sizeof(*sect);
	}
}

void	print_text32(void *ptr, void *seg, int count)
{
	int				x;
	struct section	*sect;

	x = 0;
	sect = (void *)seg;
	while (x < count)
	{
		if (ft_strcmp(sect->sectname, "__text") == 0)
		{
			ft_putendl("Contents of (__TEXT,__text) section");
			print_addresses32(ptr + sect->offset, sect);
			break ;
		}
		x++;
		sect = (void *)sect + sizeof(*sect);
	}
}

void	handle_text_section(void *ptr, void *load)
{
	struct segment_command		*seg;
	struct segment_command_64	*seg64;

	if (is_64bit(ptr))
	{
		seg64 = (void *)load;
		print_text64(ptr, (void *)load + sizeof(*seg64), seg64->nsects);
	}
	else
	{
		seg = (void *)load;
		print_text32(ptr, (void *)load + sizeof(*seg), seg->nsects);
	}
}

void	handle_sections(t_flags flags, void *ptr, void *load)
{
	if (flags.t)
		handle_text_section(ptr, load);
	if (flags.d)
		handle_data_section(ptr, load);
}

void	handle_otool(void *ptr, t_flags flags)
{
	uint32_t			x;
	uint32_t			ncmds;
	struct mach_header	*head;
	struct load_command	*load;

	head = (void *)ptr;
	if (head->magic == MH_CIGAM || head->magic == MH_CIGAM_64)
		ncmds = swap_val((void *)&head->ncmds, sizeof(head->ncmds));
	else
		ncmds = head->ncmds;
	x = (is_64bit(ptr)) ? sizeof(struct mach_header_64) : sizeof(*head);
	load = (void *)ptr + x;
	x = 0;
	while (x < ncmds)
	{
		if (load->cmd == LC_SEGMENT || load->cmd == LC_SEGMENT_64)
			handle_sections(flags, ptr, load);
		load = (void *)load + load->cmdsize;
		x++;
	}
	if (flags.h)
		print_header(ptr);
}
