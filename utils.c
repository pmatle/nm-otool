/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/04 12:31:30 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 09:31:01 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

int		is_64bit(void *ptr)
{
	unsigned int		magic;

	magic = *(unsigned int *)ptr;
	if (magic == MH_MAGIC_64 || magic == MH_CIGAM_64 || magic == FAT_MAGIC_64
			|| magic == FAT_CIGAM_64)
		return (1);
	return (0);
}

int		is_fat(void *ptr)
{
	unsigned int	magic;

	magic = *(unsigned int *)ptr;
	if (magic == FAT_MAGIC || magic == FAT_CIGAM || magic == FAT_MAGIC_64 ||
			magic == FAT_CIGAM_64)
		return (1);
	return (0);
}

int		is_a(void *ptr)
{
	if (ft_strncmp(ptr, ARMAG, SARMAG) == 0)
		return (1);
	return (0);
}

void	print_hex(long long num)
{
	if (num < 16)
	{
		if (num < 10)
			ft_putchar(num + 48);
		else
			ft_putchar(num + 87);
	}
	else
	{
		print_hex(num / 16);
		print_hex(num % 16);
	}
}

long	swap_val(unsigned char *arr, int size)
{
	int		x;
	int		shift;
	int		index;
	long	ret;

	x = 0;
	ret = 0;
	shift = 0;
	index = size - 1;
	while (x < size)
	{
		ret |= (arr[index] << shift);
		shift += 8;
		index--;
		x++;
	}
	return (ret);
}
