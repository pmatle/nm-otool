/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_otool.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 07:57:08 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 08:35:48 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void	print_address(uint64_t addr, int len)
{
	int		size;

	size = ft_intlen_base(addr, 16);
	while (size < len)
	{
		ft_putchar('0');
		size++;
	}
	if (addr != 0)
		print_hex(addr);
}

void	print_address_value(unsigned char c)
{
	if (c < 16)
		ft_putchar('0');
	print_hex(c);
	ft_putchar(' ');
}

void	print_addresses64(void *ptr, void *sect)
{
	uint64_t			x;
	uint64_t			addr;
	unsigned char		*str;
	struct section_64	*section;

	x = 0;
	section = (void *)sect;
	addr = section->addr;
	str = (unsigned char *)ptr;
	print_address(addr, 16);
	ft_putchar('\t');
	while (x < section->size)
	{
		if (x != 0 && (x % 16 == 0))
		{
			ft_putchar('\n');
			addr += 16;
			print_address(addr, 16);
			ft_putchar('\t');
		}
		print_address_value(str[x]);
		x++;
	}
	ft_putchar('\n');
}

void	print_addresses32(void *ptr, void *sect)
{
	uint32_t			x;
	uint32_t			addr;
	unsigned char		*str;
	struct section		*section;

	x = 0;
	section = (void *)sect;
	addr = section->addr;
	str = (unsigned char *)ptr;
	print_address(addr, 8);
	ft_putchar('\t');
	while (x < section->size)
	{
		if (x != 0 && (x % 16 == 0))
		{
			ft_putchar('\n');
			addr += 16;
			print_address(addr, 8);
			ft_putchar('\t');
		}
		print_address_value(str[x]);
		x++;
	}
	ft_putchar('\n');
}
