/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_data_section.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 08:36:57 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 10:02:33 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void	print_data64(void *ptr, void *seg, int count)
{
	int					x;
	struct section_64	*sect;

	x = 0;
	sect = (void *)seg;
	while (x < count)
	{
		if (ft_strcmp(sect->sectname, "__data") == 0)
		{
			ft_putendl("Contents of (__DATA,__data) section");
			print_addresses64(ptr + sect->offset, sect);
			break ;
		}
		x++;
		sect = (void *)sect + sizeof(*sect);
	}
}

void	print_data32(void *ptr, void *seg, int count)
{
	int					x;
	struct section		*sect;

	x = 0;
	sect = (void *)seg;
	while (x < count)
	{
		if (ft_strcmp(sect->sectname, "__data") == 0)
		{
			ft_putendl("Contents of (__Data,__data) section");
			print_addresses32(ptr + sect->offset, sect);
			break ;
		}
		x++;
		sect = (void *)sect + sizeof(*sect);
	}
}

void	handle_data_section(void *ptr, void *load)
{
	struct segment_command		*seg;
	struct segment_command_64	*seg64;

	if (is_64bit(ptr))
	{
		seg64 = (void *)load;
		print_data64(ptr, (void *)load + sizeof(*seg64), seg64->nsects);
	}
	else
	{
		seg = (void *)load;
		print_data32(ptr, (void *)load + sizeof(*seg), seg->nsects);
	}
}
