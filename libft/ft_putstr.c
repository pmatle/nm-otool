/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 12:54:59 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/16 09:34:17 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putstr(char const *str)
{
	ft_putstr_fd(str, 1);
}

void	ft_putcstr(char const *str, char c)
{
	int		x;

	x = 0;
	while (str[x] != c && str[x] != '\0')
	{
		ft_putchar(str[x]);
		x++;
	}
}
