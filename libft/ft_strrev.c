/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/26 16:20:57 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/19 16:46:12 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrev(char *str)
{
	int		x;
	int		y;
	char	c;

	x = 0;
	y = ft_strlen(str) - 1;
	while (y > x)
	{
		c = str[x];
		str[x] = str[y];
		str[y] = c;
		x++;
		y--;
	}
	return (str);
}

unsigned char	*ft_strurev(unsigned char *str)
{
	int				x;
	int				y;
	unsigned char	c;

	x = 0;
	y = ft_strulen(str) - 1;
	while (y > x)
	{
		c = str[x];
		str[x] = str[y];
		str[y] = c;
		x++;
		y--;
	}
	return (str);
}
