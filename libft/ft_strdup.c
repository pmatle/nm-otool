/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/22 21:05:38 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/13 10:45:27 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char	*str;
	int		x;

	str = (char*)malloc(sizeof(*str) * ft_strlen(s1) + 1);
	if (!str)
		return (NULL);
	x = 0;
	while (s1[x] != '\0')
	{
		str[x] = s1[x];
		x++;
	}
	str[x] = '\0';
	return (str);
}

char	*ft_strndup(const char *s1, int len)
{
	char	*str;
	int		x;

	x = 0;
	str = ft_strnew(len + 1);
	if (!str)
		return (NULL);
	while (x < len)
	{
		str[x] = s1[x];
		x++;
	}
	str[x] = '\0';
	return (str);
}
