/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_handling.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/08 11:30:49 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 09:12:34 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

char	**get_array64(void *ptr, char **arr, int total)
{
	int					count;
	struct section_64	*sect;

	count = 0;
	sect = (void *)ptr;
	while (count < total)
	{
		arr = array_append(arr, sect->sectname);
		count++;
		sect = (void *)sect + sizeof(*sect);
	}
	return (arr);
}

char	**get_array32(void *ptr, char **arr, int total)
{
	int					count;
	struct section		*sect;

	count = 0;
	sect = (void *)ptr;
	while (count < total)
	{
		arr = array_append(arr, sect->sectname);
		count++;
		sect = (void *)sect + sizeof(*sect);
	}
	return (arr);
}

char	**get_array(void *ptr, char **arr, int is64bit)
{
	struct segment_command		*seg;
	struct segment_command_64	*seg64;

	if (is64bit)
	{
		seg64 = (void *)ptr;
		arr = get_array64((void *)ptr + sizeof(*seg64), arr, seg64->nsects);
	}
	else
	{
		seg = (void *)ptr;
		arr = get_array32((void *)ptr + sizeof(*seg), arr, seg->nsects);
	}
	return (arr);
}

void	handle_macho_file(void *ptr, int flag, t_flags flags)
{
	if (flag)
		handle_nm(ptr);
	else
		handle_otool(ptr, flags);
}
