/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm_otool.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/04 11:16:51 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 09:13:59 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NM_OTOOL_H
# define NM_OTOOL_H

# include <stdio.h>
# include <fcntl.h>
# include <sys/mman.h>
# include <sys/stat.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <mach-o/swap.h>
# include <ar.h>
# include <mach-o/ranlib.h>
# include "libft/includes/libft.h"

typedef struct			s_flags
{
	unsigned char	h : 1;
	unsigned char	d : 1;
	unsigned char	t : 1;
}						t_flags;

typedef struct			s_map
{
	void	*ptr;
	size_t	size;
	int		flag;
	t_flags	flags;
}						t_map;

long					swap_val(unsigned char *arr, int size);
int						is_64bit(void *ptr);
int						is_fat(void *ptr);
int						is_a(void *ptr);
int						check_64(uint32_t magic);
int						is_bigendian(void *ptr);
void					free_2d(char **arr);
char					get_correct_char(char *str);
void					ft_nm(t_map data, char *name);
size_t					find_64bit(void *ptr, int size, int offset);
int						ft_arrlen(char **arr);
uint32_t				get_value(void *ptr, unsigned char *bytes, int size);
void					print_hex(long long);
uint64_t				convert_to_little(void *ptr, int size);
void					print_value64(uint64_t value, char c);
void					print_value32(uint32_t value, char c);
int						check_invalid(t_map *data, char *str);
int						get_file_ptr(char *str, t_map *data);
int						ft_intlen_base(long long n, int base);
void					handle_macho_file(void *ptr, int flag, t_flags flags);
void					print_symbolname32(char *table, int x, void *ptr);
void					print_symbolname64(char *table, int x, void *ptr);
int						handle_fat_files(t_map data, char *name);
void					handle_ar_file(void *ptr, int size, char *name,
		t_map data);
char					**array_append(char **arr, char *str);
void					handle_symtab(void *ptr, int is64bit, void *top,
		char **arr);
char					**get_array(void *ptr, char **arr, int is64bit);
void					print_address(uint64_t addr, int len);
void					print_addresses64(void *ptr, void *sect);
void					print_addresses32(void *ptr, void *sect);
void					handle_data_section(void *ptr, void *load);
void					print_header(void *ptr);
void					handle_nm(void *ptr);
void					handle_otool(void *ptr, t_flags flags);

#endif
