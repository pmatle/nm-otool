/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils1.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/17 15:25:18 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/19 11:26:30 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

int			is_bigendian(void *ptr)
{
	uint32_t	magic;

	magic = *(uint32_t *)ptr;
	if (magic == MH_CIGAM || magic == MH_CIGAM_64)
		return (1);
	return (0);
}

uint32_t	get_value(void *ptr, unsigned char *bytes, int size)
{
	uint32_t	value;

	if (is_bigendian(ptr))
		value = swap_val(bytes, size);
	else
		value = *(uint32_t *)bytes;
	return (value);
}

char		get_correct_char(char *str)
{
	if (ft_strcmp(str, "__text") == 0)
		return ('t');
	else if (ft_strcmp(str, "__data") == 0)
		return ('d');
	else if (ft_strcmp(str, "__bss") == 0)
		return ('b');
	return ('s');
}

int			check_64(uint32_t magic)
{
	if (magic == MH_MAGIC || magic == MH_CIGAM)
		return (1);
	return (0);
}

void		print_symbolname64(char *table, int x, void *ptr)
{
	struct nlist_64			*list;

	list = (void *)ptr;
	ft_putchar(' ');
	if (list[x].n_sect == NO_SECT && list[x].n_type & N_INDR)
	{
		ft_putstr(table + list[x].n_un.n_strx);
		if (list[x].n_value != 0 && list[x].n_value < 0xfffff)
		{
			ft_putstr(" (indirect for ");
			ft_putstr(table + list[x].n_value);
			ft_putstr(")");
		}
		ft_putchar('\n');
	}
	else
		ft_putendl(table + list[x].n_un.n_strx);
}
