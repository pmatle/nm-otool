/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   otool_header.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/19 18:47:17 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 09:47:31 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void	print_val(uint32_t val, int base, int spaces)
{
	int		len;
	int		x;

	len = ft_intlen_base(val, base);
	x = spaces - len;
	while (x > 0)
	{
		ft_putchar(' ');
		x--;
	}
	if (base == 16)
	{
		ft_putstr("0x");
		print_hex(val);
	}
	else
		ft_putnbr(val);
}

void	print_flags(uint32_t num, int swap)
{
	int				x;
	unsigned char	*arr;

	x = 3;
	ft_putstr("  0x");
	arr = (unsigned char *)&num;
	if (swap)
		arr = ft_strurev(arr);
	while (x >= 0)
	{
		if (arr[x] < 16)
			ft_putchar('0');
		print_hex(arr[x]);
		x--;
	}
}

void	print_caps(void *ptr)
{
	unsigned char	*str;
	int				size;

	str = (void *)ptr + 2;
	ft_putstr("  0x");
	if (str[0] != 0)
		print_hex(str[0]);
	size = ft_intlen_base(str[1], 10);
	if (size < 1)
		ft_putchar('0');
	print_hex(str[1]);
}

void	print_header(void *ptr)
{
	struct mach_header	*head;
	int					size;

	head = (void *)ptr;
	ft_putendl("Mach header");
	ft_putstr("      magic");
	ft_putstr(" cputype   ");
	ft_putstr(" cpusubtype ");
	ft_putstr(" caps  ");
	ft_putstr(" filetype  ");
	ft_putstr(" ncmds ");
	ft_putstr(" sizeofcmds ");
	ft_putendl(" flags");
	print_val(head->magic, 16, 9);
	print_val(head->cputype, 10, 9);
	print_val((int16_t)head->cpusubtype, 10, 13);
	size = sizeof(head->magic) + sizeof(head->cputype);
	print_caps(ptr + size);
	print_val(head->filetype, 10, 11);
	print_val(head->ncmds, 10, 8);
	print_val(head->sizeofcmds, 10, 12);
	print_flags(head->flags, 1);
	ft_putchar('\n');
}
