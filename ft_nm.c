/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/19 11:38:57 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/19 18:16:38 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void	handle_nm(void *ptr)
{
	uint32_t			x;
	uint32_t			ncmds;
	struct mach_header	*head;
	struct load_command	*load;
	char				**arr;

	arr = NULL;
	head = (void *)ptr;
	if (head->magic == MH_CIGAM || head->magic == MH_CIGAM_64)
		ncmds = swap_val((void *)&head->ncmds, sizeof(head->ncmds));
	else
		ncmds = head->ncmds;
	x = (is_64bit(ptr)) ? sizeof(struct mach_header_64) : sizeof(*head);
	load = (void *)ptr + x;
	x = 0;
	while (x < ncmds)
	{
		if (load->cmd == LC_SEGMENT_64 || load->cmd == LC_SEGMENT)
			arr = get_array(load, arr, is_64bit(ptr));
		if (load->cmd == LC_SYMTAB)
			handle_symtab(load, is_64bit(ptr), ptr, arr);
		load = (void *)load + load->cmdsize;
		x++;
	}
	free_2d(arr);
}
