/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_fat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 08:28:53 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 09:37:18 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void	no64bit(t_map data, void *ptr, int swap)
{
	struct fat_arch		*arch;
	uint32_t			offset;

	arch = (void *)ptr;
	if (swap)
		offset = swap_val((unsigned char *)&arch->offset, sizeof(uint32_t));
	else
		offset = arch->offset;
	offset += find_64bit(data.ptr, data.size, offset);
	handle_macho_file(data.ptr + offset, data.flag, data.flags);
}

void	handle_correct(t_map data, uint32_t count, int swap)
{
	uint32_t			x;
	uint32_t			offset;
	struct mach_header	*head;
	struct fat_arch		*arch;
	struct fat_arch		*tmp;

	x = 0;
	arch = (void *)data.ptr + sizeof(struct fat_header);
	tmp = arch;
	while (x < count)
	{
		if (swap)
			offset = swap_val((unsigned char *)&arch->offset, sizeof(uint32_t));
		else
			offset = arch->offset;
		head = (void *)data.ptr + offset;
		if (is_64bit(head))
		{
			handle_macho_file(head, data.flag, data.flags);
			return ;
		}
		tmp = (void *)tmp + sizeof(*tmp);
		x++;
	}
	no64bit(data, arch, swap);
}

void	handle_fat32(t_map data, int swap, char *name, uint32_t num)
{
	uint32_t			offset;
	struct fat_arch		*arch;
	t_map				tmp;

	arch = (void *)data.ptr + sizeof(struct fat_header);
	if (swap)
		offset = swap_val((unsigned char *)&arch->offset, sizeof(uint32_t));
	else
		offset = arch->offset;
	if (is_a(data.ptr + offset))
	{
		tmp.size = data.size;
		tmp.ptr = data.ptr + offset;
		handle_ar_file(data.ptr + offset, data.size, name, data);
	}
	else
		handle_correct(data, num, swap);
}

void	handle_fat64(t_map data, int swap, char *name, uint32_t num)
{
	uint64_t			offset;
	struct fat_arch_64	*arch;
	t_map				tmp;

	arch = (void *)data.ptr + sizeof(struct fat_header);
	if (swap)
		offset = swap_val((unsigned char *)&arch->offset, sizeof(uint64_t));
	else
		offset = arch->offset;
	if (is_a(data.ptr + offset))
	{
		tmp.size = data.size + offset;
		tmp.ptr = data.ptr + offset;
		handle_ar_file(data.ptr + offset, data.size, name, data);
	}
	else
	{
		handle_correct(data, num, swap);
	}
}

int		handle_fat_files(t_map data, char *name)
{
	struct fat_header	*head;
	uint32_t			num_arch;
	int					swap;

	swap = 0;
	head = (void *)data.ptr;
	if (head->magic == FAT_CIGAM || head->magic == FAT_CIGAM_64)
	{
		swap = 1;
		num_arch = swap_val((unsigned char *)&head->nfat_arch,
				sizeof(uint32_t));
	}
	if (head->magic == FAT_MAGIC || head->magic == FAT_CIGAM)
		handle_fat32(data, swap, name, num_arch);
	else
		handle_fat64(data, swap, name, num_arch);
	return (1);
}
