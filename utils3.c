/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/19 11:07:25 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/19 15:42:32 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

size_t		find_64bit(void *ptr, int size, int offset)
{
	int		count;

	ptr = ptr + offset;
	count = offset;
	while (count < size)
	{
		count++;
		if ((count + 4) == size)
			break ;
		ptr++;
		if (is_64bit(ptr))
			return (count - offset);
	}
	return (0);
}

uint64_t	convert_to_little(void *ptr, int size)
{
	uint16_t	val16;
	uint32_t	val32;
	uint64_t	val64;

	if (size == 2)
	{
		val16 = swap_val(ptr + 2, sizeof(int16_t));
		return (val16);
	}
	else if (size == 4)
	{
		val32 = swap_val(ptr, sizeof(int32_t));
		return (val32);
	}
	else if (size == 8)
	{
		val64 = swap_val(ptr, sizeof(int64_t));
		return (val64);
	}
	return (0);
}

void		print_symbolname32(char *table, int x, void *ptr)
{
	struct nlist		*list;

	list = (void *)ptr;
	ft_putchar(' ');
	if (list[x].n_sect == NO_SECT && list[x].n_type & N_INDR)
	{
		ft_putstr(table + list[x].n_un.n_strx);
		if (list[x].n_value != 0 && list[x].n_value < 0xfffff)
		{
			ft_putstr(" (indirect for ");
			ft_putstr(table + list[x].n_value);
			ft_putstr(")");
		}
		ft_putchar('\n');
	}
	else
		ft_putendl(table + list[x].n_un.n_strx);
}
