# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmatle <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/14 08:30:50 by pmatle            #+#    #+#              #
#    Updated: 2018/09/20 09:18:00 by pmatle           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME1 = ft_otool

NAME2 = ft_nm

SRCS = file_parsing.c utils.c handle_symtab.c file_handling.c \
	   print_table.c array_append.c handle_fat.c handle_ar.c utils1.c utils3.c \
	   otool_header.c print_otool.c print_data_section.c 

SRCS2 = ft_nm.c nm.c

SRCS1 = ft_otool.c otool.c

OBJS = file_parsing.o utils.o handle_symtab.o file_handling.o \
	   print_table.o array_append.o handle_fat.o handle_ar.o utils1.o utils3.o \
	   ft_otool.o ft_nm.o otool_header.o print_otool.o print_data_section.o

CC = gcc

CFLAGS = -Wall -Wextra -Werror 

all: $(NAME2) $(NAME1)
	$(CC) $(CFLAGS) $(OBJS) otool.o -L libft -lft -o $(NAME1)
	$(CC) $(CFLAGS) $(OBJS) nm.o -L libft -lft -o $(NAME2)

$(NAME1):
	@make re -C libft
	@$(CC) $(CFLAGS) -c $(SRCS) $(SRCS1)

$(NAME2):
	@make re -C libft
	@$(CC) $(CFLAGS) -c $(SRCS) $(SRCS2)

clean:
	@/bin/rm -rf $(OBJS) otool.o nm.o
	@make clean -C libft

fclean: clean
	@/bin/rm -rf $(NAME2)
	@/bin/rm -rf $(NAME1)
	@make fclean -C libft

re: fclean all
