/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_parsing.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/04 14:43:53 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 09:29:15 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

int		file_error(char *str)
{
	ft_putstr_fd("Failed to open ", 2);
	ft_putendl_fd(str, 2);
	return (-1);
}

int		check_invalid(t_map *data, char *str)
{
	struct mach_header	*head;

	head = (void *)data->ptr;
	if (is_a(data->ptr) || is_fat(data->ptr) ||
			(head->filetype >= 1 && head->filetype <= 11))
		return (1);
	ft_putstr_fd(str, 2);
	ft_putendl_fd(": The file was not recognized as a valid object file", 2);
	munmap(data->ptr, data->size);
	return (-1);
}

int		get_file_ptr(char *str, t_map *data)
{
	int				fd;
	struct stat		buff;

	if ((fd = open(str, O_RDONLY)) < 0)
		return (file_error(str));
	if (fstat(fd, &buff) < 0)
		return (file_error(str));
	data->ptr = mmap(0, buff.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (data->ptr == MAP_FAILED)
		return (file_error(str));
	data->size = buff.st_size;
	return (check_invalid(data, str));
}
