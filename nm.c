/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/04 11:12:47 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 10:07:10 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void	ft_nm(t_map data, char *name)
{
	data.flag = 1;
	if (is_a(data.ptr))
		handle_ar_file(data.ptr, data.size, name, data);
	else if (is_fat(data.ptr))
		handle_fat_files(data, name);
	else
		handle_macho_file(data.ptr, 1, data.flags);
}

int		multiple_files(char **arr, int count)
{
	int		x;
	t_map	data;

	x = 1;
	while (x < count)
	{
		if (get_file_ptr(arr[x], &data) < 0)
		{
			x++;
			continue ;
		}
		ft_putchar('\n');
		ft_putstr(arr[x]);
		ft_putendl(":");
		ft_nm(data, arr[x]);
		munmap(data.ptr, data.size);
		x++;
	}
	return (1);
}

int		main(int argc, char **argv)
{
	t_map	data;

	if (argc < 2)
	{
		if (get_file_ptr("a.out", &data) < 0)
			return (-1);
		ft_nm(data, "a.out");
		munmap(data.ptr, data.size);
	}
	else if (argc == 2)
	{
		if (get_file_ptr(argv[1], &data) < 0)
			return (-1);
		ft_nm(data, argv[1]);
		munmap(data.ptr, data.size);
	}
	else if (argc > 2)
		multiple_files(argv, argc);
	return (0);
}
