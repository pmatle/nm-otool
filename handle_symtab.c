/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_symtab.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/08 14:15:20 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/19 11:39:35 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

char	print_letter(int type, char c)
{
	if (type & N_EXT)
		return (c - 32);
	else
		return (c);
}

char	print_section(int sect, int type, uint64_t value, char **arr)
{
	char	c;

	c = 'U';
	if (sect == NO_SECT)
	{
		if (type & N_INDR && (value != 0 && value < 0xfffff))
			c = print_letter(type, 'i');
		else if (type & N_ABS)
			c = print_letter(type, 'a');
		else if (type & N_UNDF)
			c = print_letter(type, 'c');
	}
	else
	{
		if (type & N_TYPE)
		{
			if (type & N_SECT)
				c = print_letter(type, get_correct_char(arr[sect - 1]));
			else if (type & N_INDR)
				c = print_letter(type, 'i');
		}
	}
	return (c);
}

void	handle_symtab_64(char *ptr, struct symtab_command *sym, char **arr)
{
	struct nlist_64		*list;
	char				*strtable;
	uint32_t			max;
	uint32_t			x;
	char				c;

	x = 0;
	list = (void *)ptr + get_value(ptr, (void *)&sym->symoff,
			sizeof(sym->symoff));
	strtable = (void *)ptr + get_value(ptr, (void *)&sym->stroff,
			sizeof(sym->stroff));
	max = get_value(ptr, (void *)&sym->nsyms, sizeof(sym->nsyms));
	while (x < max)
	{
		if (!(list[x].n_type & N_STAB))
		{
			c = print_section(list[x].n_sect, list[x].n_type, list[x].n_value,
					arr);
			print_value64(list[x].n_value, c);
			ft_putchar(c);
			print_symbolname64(strtable, x, list);
		}
		x++;
	}
}

void	handle_symtab_32(char *ptr, struct symtab_command *sym, char **arr)
{
	struct nlist	*list;
	char			*strtable;
	uint32_t		max;
	uint32_t		x;
	char			c;

	x = 0;
	list = (void *)ptr + get_value(ptr, (void *)&sym->symoff,
			sizeof(sym->symoff));
	strtable = (void *)ptr + get_value(ptr, (void *)&sym->stroff,
			sizeof(sym->stroff));
	max = get_value(ptr, (void *)&sym->nsyms, sizeof(sym->nsyms));
	while (x < max)
	{
		if (!(list[x].n_type & N_STAB))
		{
			c = print_section(list[x].n_sect, list[x].n_type, list[x].n_value,
					arr);
			print_value32(list[x].n_value, c);
			ft_putchar(c);
			print_symbolname32(strtable, x, list);
		}
		x++;
	}
}

void	handle_symtab(void *ptr, int is64bit, void *top, char **arr)
{
	struct symtab_command	*sym;

	sym = (struct symtab_command *)ptr;
	if (is64bit)
		handle_symtab_64(top, sym, arr);
	else
		handle_symtab_32(top, sym, arr);
}
