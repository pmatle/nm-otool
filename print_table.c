/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_table.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/08 16:55:31 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/19 17:40:41 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

int		ft_intlen_base(long long n, int base)
{
	int		x;

	x = 0;
	if (n < 0 && base == 10)
	{
		x++;
		n = n * -1;
	}
	while (n)
	{
		x++;
		n = n / base;
	}
	return (x);
}

void	print_value64(uint64_t value, char c)
{
	int		size;

	if (c != 'I' && c != 'i' && c != 'U')
	{
		size = ft_intlen_base(value, 16);
		while (size < 16)
		{
			ft_putchar('0');
			size++;
		}
		if (value != 0)
			print_hex(value);
	}
	else
		ft_putstr("                ");
	ft_putchar(' ');
}

void	print_value32(uint32_t value, char c)
{
	int				size;

	if (c != 'I' && c != 'i' && c != 'U')
	{
		size = ft_intlen_base(value, 16);
		while (size < 8)
		{
			ft_putchar('0');
			size++;
		}
		if (value != 0)
			print_hex(value);
	}
	else
		ft_putstr("        ");
	ft_putchar(' ');
}
