/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array_append.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/11 11:19:08 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/11 15:53:15 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

int		ft_arrlen(char **arr)
{
	int		x;

	x = 0;
	while (arr[x] != NULL)
	{
		x++;
	}
	return (x);
}

char	**ft_arrcpy(char **dst, char **src, int len)
{
	int		x;

	x = 0;
	while (x < len)
	{
		dst[x] = ft_strdup(src[x]);
		if (!dst[x])
			return (NULL);
		x++;
	}
	return (dst);
}

void	free_2d(char **arr)
{
	int		x;

	x = 0;
	while (arr[x])
	{
		free(arr[x]);
		x++;
	}
	free(arr);
}

char	**append(char **arr, char *str)
{
	int		arr_len;
	char	**final;

	arr_len = ft_arrlen(arr) + 1;
	final = (char **)malloc(sizeof(*final) * arr_len + 1);
	if (!final)
		return (NULL);
	final = ft_arrcpy(final, arr, arr_len - 1);
	final[arr_len - 1] = ft_strdup(str);
	if (!final[arr_len - 1])
	{
		free_2d(final);
		return (NULL);
	}
	final[arr_len] = NULL;
	free_2d(arr);
	arr = NULL;
	return (final);
}

char	**array_append(char **arr, char *str)
{
	char	**final;
	int		arr_len;
	int		len;
	int		x;

	x = 0;
	arr_len = 0;
	len = ft_strlen(str);
	if (arr == NULL)
	{
		final = (char **)malloc(sizeof(*final) * 2);
		final[0] = ft_strdup(str);
		final[1] = NULL;
		return (final);
	}
	return (append(arr, str));
}
