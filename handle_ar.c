/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_ar.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 10:02:46 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 09:10:39 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

int		is_magic(void *ptr)
{
	uint32_t	magic;

	magic = *(uint32_t *)ptr;
	if (magic == MH_MAGIC_64 ||
			magic == MH_CIGAM_64)
		return (1);
	return (0);
}

void	print_details(char *ptr, char *name)
{
	ft_putchar('\n');
	ft_putstr(name);
	ft_putchar('(');
	ft_putstr(ptr);
	ft_putendl("):");
}

int		print_name(char *ptr, char *name)
{
	static int	count = 0;
	static char	*tmp = NULL;

	while (*ptr != '`')
	{
		ptr--;
	}
	ptr += 2;
	if (tmp != NULL)
	{
		if (ft_strncmp(ptr, tmp, ft_strlen(tmp)) == 0)
		{
			count = 0;
			ft_strdel(&tmp);
			return (1);
		}
	}
	if (count == 0)
		tmp = ft_strdup(ptr);
	print_details(ptr, name);
	count++;
	return (0);
}

void	handle_ar_file(void *ptr, int size, char *name, t_map data)
{
	int			x;
	int			stop;

	x = 0;
	while (x < size)
	{
		if ((x + 4) == size)
			break ;
		if (is_magic(ptr))
		{
			stop = print_name(ptr, name);
			if (stop)
				break ;
			handle_macho_file(ptr, data.flag, data.flags);
		}
		ptr++;
		x++;
	}
}
