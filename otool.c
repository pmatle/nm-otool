/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   otool.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/17 18:16:45 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/20 10:08:39 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm_otool.h"

void	ft_otool(t_map data, char *name)
{
	data.flag = 0;
	if (is_a(data.ptr))
		handle_ar_file(data.ptr, data.size, name, data);
	else if (is_fat(data.ptr))
		handle_fat_files(data, name);
	else
		handle_macho_file(data.ptr, 0, data.flags);
}

int		is_flags(char *str, t_flags *flags)
{
	int		x;
	int		ret;

	ret = 0;
	if (str[0] == '-')
	{
		x = 1;
		while (str[x] != '\0')
		{
			if (str[x] == 't')
				flags->t = 1;
			else if (str[x] == 'd')
				flags->d = 1;
			else if (str[x] == 'h')
				flags->h = 1;
			else
				return (-1);
			x++;
		}
		if (flags->h || flags->t || flags->d)
			ret = 1;
	}
	return (ret);
}

void	init_flags(t_flags *flags)
{
	flags->t = 0;
	flags->d = 0;
	flags->h = 0;
}

int		check_flags(t_map *data, char *str)
{
	int		ret;

	ret = is_flags(str, &data->flags);
	if (ret == -1)
		ft_putendl_fd("Error: Invalid flag detected", 2);
	else if (ret == 0)
		data->flags.t = 1;
	return (ret);
}

int		main(int argc, char **argv)
{
	int		x;
	t_map	data;

	init_flags(&data.flags);
	if (argc > 1)
	{
		if ((x = check_flags(&data, argv[1]) == -1))
			return (-1);
		x += 2;
		while (x < argc)
		{
			if (get_file_ptr(argv[x], &data) < 0)
			{
				x++;
				continue ;
			}
			ft_putstr(argv[x]);
			ft_putendl(":");
			ft_otool(data, argv[x]);
			munmap(data.ptr, data.size);
			x++;
		}
	}
	return (1);
}
